import { shallowMount } from '@vue/test-utils'
import FooCalculator from '@/components/FooCalculator.vue'

describe('Test FooCalculator component', () => {
    
    test('FooCalculator is render instance correctly', () => {
        const wrapper = shallowMount(FooCalculator)
        
        expect(wrapper.isVueInstance()).toBe(true)
    })

    test('Fill Number in Input correctly', () => {
        const wrapper = shallowMount(FooCalculator)

        wrapper.find('#firstInput').setValue(6)
        wrapper.find('#secondInput').setValue(4)

        expect(wrapper.vm.firstNumber).toBe(6)
        expect(wrapper.vm.secondNumber).toBe(4)
    })

    test('Number sum correctly', async () => {
        const wrapper = shallowMount(FooCalculator)

        wrapper.find('#firstInput').setValue(6)
        wrapper.find('#secondInput').setValue(4)

        await wrapper.vm.$nextTick()

        expect(wrapper.find('#sumResult').text()).toBe('10')
    })
})